package main

import (
  "fmt"

  "log"
  "os"
  "io"

  "time"

  "net"
  "net/http"
  "html/template"

  "strconv"

  "io/ioutil"

  "context"
  "docker.io/go-docker"
  "docker.io/go-docker/api/types"
  "docker.io/go-docker/api/types/container"
)

func main() {
  http.HandleFunc("/python", handler)
  if os.Getenv("LISTEN_PID") == strconv.Itoa(os.Getpid()) {
    file := os.NewFile(3, "socket")
    sock, err := net.FileListener(file)
    if err != nil { panic(err) }
    log.Fatal(http.Serve(sock, nil))
  } else {
    log.Fatal(http.ListenAndServe(":80", nil))
  }
}

type Response struct {
  Code string
  StdIn string
  Output string
}

func handler(w http.ResponseWriter, r *http.Request) {
  isError := func (err error) (bool) {
    if err == nil { return false }
    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte(fmt.Sprintf("Error occured: %s\n", err)))
    return true
  }

  r.ParseForm()
  code := r.Form.Get("code")
  stdin := r.Form.Get("stdin")

  t, err := template.ParseFiles("python.html")
  if isError(err) { return }

  attr := Response{Code: code, StdIn: stdin, Output: runPython(code, stdin)}
  err = t.Execute(w, attr)
  if isError(err) { return }
}

func runPython(code string, stdin string) (string) {
  if code == "" {
	  return ""
  }

  isError := func (err error) (bool) {
    if err == nil { return false }
    fmt.Printf("Error occured: %s\n", err)
    return true
  }

  tempdir, err := ioutil.TempDir("", "python-run")
  if isError(err) { return "Internal Server Error: Try Again" }
  defer os.RemoveAll(tempdir)

  err = ioutil.WriteFile(tempdir+"/code.py", []byte(code), 0755)
  if isError(err) { return "Internal Server Error: Try Again" }

  ctx := context.Background()
  cli, err := docker.NewEnvClient()
  if isError(err) { return "Internal Server Error: Try Again" }

  con, err := cli.ContainerCreate(ctx, &container.Config{
    Image: "python:3",
    NetworkDisabled: true,
    OpenStdin: true, StdinOnce: true,
    WorkingDir: "/run/app",
    Cmd: []string{"python", "code.py"},
  }, &container.HostConfig{
    Binds: []string{tempdir+":/run/app"},
    Resources: container.Resources{
      CPUQuota: 10000,
      Memory: 33554432,
    },
    LogConfig: container.LogConfig{Type: "none"},
  }, nil, "")
  if isError(err) { return "Internal Server Error: Try Again" }
  defer cli.ContainerRemove(ctx, con.ID, types.ContainerRemoveOptions{})

  err = cli.ContainerStart(ctx, con.ID, types.ContainerStartOptions{})
  if isError(err) { return "Internal Server Error: Try Again" }

  hr, err := cli.ContainerAttach(ctx, con.ID, types.ContainerAttachOptions{Stream: true, Stdin: true, Stdout: true, Stderr: true})
  if isError(err) { return "Internal Server Error: Try Again" }

  _, err = hr.Conn.Write([]byte(stdin))
  if isError(err) { return "Internal Server Error: Try Again" }
  err = hr.CloseWrite()
  if isError(err) { return "Internal Server Error: Try Again" }

  ctxTimeout, cancel := context.WithTimeout(ctx, 3*time.Second)
  statusCh, errCh := cli.ContainerWait(ctxTimeout, con.ID, container.WaitConditionNotRunning)
  select {
  case err := <-errCh:
    if err.Error() == "context deadline exceeded" {
      data, err := cli.ContainerInspect(ctx, con.ID)
      if isError(err) { return "Internal Server Error: Try Again" }

      if data.State.Status == "running" {
        err = cli.ContainerKill(ctx, con.ID, "KILL")
        if isError(err) { return "Internal Server Error: Try Again" }
      }

      return "Timeout exceeded on program"
    }
    if isError(err) { return "Internal Server Error: Try Again" }
  case <- statusCh:
  }
  cancel()

  //data, err := cli.ContainerInspect(ctx, con.ID)
  //if isError(err) { return "Internal Server Error: Try Again" }

  //if data.State.Status == "running" {
  //}

  //reader, err := cli.ContainerLogs(ctx, con.ID, types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true})
  //if isError(err) { return "Internal Server Error: Try Again" }

  resp := ""
  for true {
    header := make([]byte, 8)
    n, err := hr.Conn.Read(header)
    if n == 0 { break }
    if isError(err) { return "Internal Server Error: Try Again" }

    length := int(header[7]) + 1 << 8 * int(header[6]) + 1 << 16 * int(header[5]) + 1 << 24 * int(header[4])

    data := make([]byte, length)
    _, err = hr.Conn.Read(data)
    if err == io.EOF { break }
    if isError(err) { return "Internal Server Error: Try Again" }

    resp += string(data)
  }

  err = hr.Conn.Close()
  if isError(err) { return "Internal Server Error: Try Again" }

  return resp
}
